#!/usr/bin/env bash

source .env

# Create a systemd service that autostarts & manages a docker-compose instance in the current directory
# by Uli Köhler - https://techoverflow.net
# Licensed as CC0 1.0 Universal
SERVICENAME=$(basename $(pwd))

echo "Creating systemd service... /etc/systemd/system/${SERVICENAME}.service"
# Create systemd service file
sudo cat >/etc/systemd/system/$SERVICENAME.service <<EOF
[Unit]
Description=$SERVICENAME
Requires=docker.service network-online.target
After=docker.service network-online.target

[Service]
Restart=always
User=synapse
Group=docker
WorkingDirectory=$(pwd)
# Shutdown container (if running) when unit is started
ExecStartPre=$(which docker-compose) -f docker-compose.yml down
# Start container when unit is started
ExecStart=$(which docker-compose) -f docker-compose.yml up
# Stop container when unit is stopped
ExecStop=$(which docker-compose) -f docker-compose.yml down

[Install]
WantedBy=multi-user.target
EOF
echo "Service file contents:"
cat /etc/systemd/system/$SERVICENAME.service
echo "Enabling & starting $SERVICENAME"
# Autostart systemd service
sudo systemctl enable $SERVICENAME.service

# Build Synapse container
$(which docker-compose) build matrix

# Generate Synapse homeserver config from template
$(which docker-compose) run matrix migrate_config
$(which docker-compose) run matrix generate

# Start systemd service now
sudo systemctl start $SERVICENAME.service || exit 0 # don't fail to provision if the service can't be started. 

