# Synapse Stack

Docker Compose files for a turn-key Matrix Synapse server.
 #### Features
- Nginx reverse proxy on the frontend.
- Postgresql on the backend.
- Segmented networks. Only Nginx can talk to Synapse. Only Synapse can talk to Postgresql. 
- S3 media storage provider. Stores uploaded content to private S3 bucket. 
- `url_preview_enable` can be set through the `URL_PREVIEW_ENABLED` environment variable.


## Installation

Clone this repo
```bash
git clone https://gitlab.com/soupandrifles/synapse-stack.git
cd synapse-stack
```
Create a `.env` file

(Note: If deploying with [Synapse-infra-tf](https://gitlab.com/soupandrifles/synapse-infra-tf) you should skip this step and follow the instruction in the [README](https://gitlab.com/soupandrifles/synapse-infra-tf/-/blob/main/README.md).)
```bash
mkdir pgdata
cat << EOF >> .env
export PDDATA="./pgdata"
export SYNAPSE_SERVER_NAME="example.com"
export AWS_ACCESS_KEY_ID=<secret>
export AWS_SECRET_ACCESS_KEY=<secret>
export POSTGRES_PASSWORD=<secret>
export AWS_REGION="us-east-1"
export LE_DOMAINS="example.com, www.example.com, matrix.example.com"
export LE_EMAIL="admin@example.com"
EOF
```

## Usage

```bash
$ docker-compose up
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.