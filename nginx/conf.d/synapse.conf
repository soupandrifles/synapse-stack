upstream synapse_container {
    server matrix:8008;
}
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    # For the federation port
    listen 8448 ssl http2 default_server;
    listen [::]:8448 ssl http2 default_server;
    
    include conf.d/certificate.conf;

    server_name soupandriflesco.org matrix.soupandriflesco.org;

    # Redirect to website
    location = / {
        return 301 https://www.soupandriflesco.org$request_uri;
    }

    location ~ ^(/_matrix|/_synapse/client) {
        # note: do not add a path (even a single /) after the port in `proxy_pass`,
        # otherwise nginx will canonicalise the URI and cause signature verification
        # errors.
        proxy_pass http://synapse_container;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;

        # Nginx by default only allows file uploads up to 1M in size
        # Increase client_max_body_size to match max_upload_size defined in homeserver.yaml
        client_max_body_size 50M;
    }

    location ~ ^/.well-known/matrix/server {
        default_type application/json;
        return 200 '{"m.server": "matrix.soupandriflesco.org"}';
    }
}